<?php

require_once __DIR__.'/vendor/autoload.php';
use WebJump\Rotas;
$rotas = new Rotas();

$rotas->get('/', function () {
    return WebJump\Controller\WebJump::index();
});

$rotas->get('products', function () {
    return WebJump\Controller\WebJump::products();
});

$rotas->get('addProduct', function () {
    return WebJump\Controller\WebJump::addProduct();
});

$rotas->post('cadastra', function () {
    return WebJump\Controller\Produtos_Form::createForm();
});

$rotas->post('deleta', function () {
    return WebJump\Model\Produtos_Crud::deleta();
});

$rotas->get('edita', function () {
    return WebJump\Controller\WebJump::edita();
});

$rotas->post('edita_produto', function () {
    return WebJump\Controller\Produtos_Form::editaForm();
});

$rotas->post('seleciona_filtro', function () {
    return WebJump\Model\Produtos_Crud::seleciona_filtro();
});

$rotas->get('categories', function () {
    return WebJump\Controller\WebJump::categories();
});

$rotas->get('addCategory', function () {
    return WebJump\Controller\WebJump::addCategory();
});

$rotas->post('cadastra_categoria', function () {
    return WebJump\Controller\Categoria_Form::createForm();
});

$rotas->post('deleta_categoria', function () {
    return WebJump\Model\Categorias_Crud::deleta();
});

$rotas->get('edita_categoria', function () {
    return WebJump\Controller\WebJump::edita_categoria();
});

$rotas->post('seleciona_filtro_categoria', function () {
    return WebJump\Model\Categorias_Crud::seleciona_filtro();
});

$rotas->post('atualiza_categoria', function () {
    return WebJump\Controller\Categoria_Form::editaForm();
});

$rotas->run();

