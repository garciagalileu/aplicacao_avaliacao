<?php

namespace WebJump\Model;
require('vendor/database.php');
require('vendor/config.php');

class Categorias_Crud {

    public function insere($dados) {

        $pdo = new \PDO("mysql:host=".HOST.";"."dbname=".NOME_BANCO, SENHA_BANCO, "");
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $categoriasDAO = new CategoriasDAO($pdo);
        
        if ($categoriasDAO->insert($dados)) {
            header("Location: ". BASE_URL.'?r=categories');
        } else {
            header("Location: ". BASE_URL.'?r=addCategory');
        }
    }
    
    public function atualiza($dados) {

        $pdo = new \PDO("mysql:host=".HOST.";"."dbname=".NOME_BANCO, SENHA_BANCO, "");
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $categoriasDAO = new CategoriasDAO($pdo);
        
        if ($categoriasDAO->update($dados)) {
            header("Location: ". BASE_URL.'?r=categories');
        } else {
            header("Location: ". BASE_URL);
        }
    }
    
    public function seleciona() {

        $pdo = new \PDO("mysql:host=".HOST.";"."dbname=".NOME_BANCO, SENHA_BANCO, "");
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $categoriasDAO = new CategoriasDAO($pdo);
        
        return $categoriasDAO->select($n = null);
       
    }
    
    public function deleta() {
       
        $pdo = new \PDO("mysql:host=".HOST.";"."dbname=".NOME_BANCO, SENHA_BANCO, "");
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $categoriasDAO = new CategoriasDAO($pdo);
        
        $id = filter_input_array(INPUT_POST)['id'];

        if ($categoriasDAO->delete($id)) {
            $data = array('situacao' => 'true');
            echo json_encode($data);
        } else {
            $data = array('situacao' => 'false');
            echo json_encode($data);
        }
        
    }

    public function seleciona_filtro() {
       
        $pdo = new \PDO("mysql:host=".HOST.";"."dbname=".NOME_BANCO, SENHA_BANCO, "");
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $categoriasDAO = new CategoriasDAO($pdo);
        
        $id = filter_input_array(INPUT_POST)['id'];
  
        if ($categoriasDAO->select($id)) {
            session_start();
            $_SESSION['categorias'] = $categoriasDAO->select($id);
            $data = array('situacao' => 'true');
            echo json_encode($data);
        } else {
            $data = array('situacao' => 'false');
            echo json_encode($data);
        }
        
    }
}
