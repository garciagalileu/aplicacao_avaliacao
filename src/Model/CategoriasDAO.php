<?php

namespace WebJump\Model;

class CategoriasDAO {

    private $db;

    public function __construct(\PDO $pdo) {
        $this->db = $pdo;
    }

    public function insert($dados) {

        $stmt = $this->db->prepare("INSERT INTO categorias SET nome = :nome, codigo = :codigo");
        $stmt->bindValue(':nome',$dados['nome']);
        $stmt->bindValue(':codigo',$dados['codigo']);
        return $stmt->execute();
    }
    public function select($n) {
        $result = ($n == null) ? $this->db->query("SELECT * FROM categorias;")->fetchAll() : $this->db->query("SELECT * FROM categorias WHERE id ='".$n."'")->fetchAll();
        return $result;
    }
    
    public function update($dados) {
        session_start();
        $stmt = $this->db->prepare("UPDATE categorias SET nome = :nome, codigo = :codigo WHERE id = :id");
        $stmt->bindValue(':id', $_SESSION['categorias'][0]['id']);
        $stmt->bindValue(':nome',$dados['nome']);
        $stmt->bindValue(':codigo',$dados['codigo']);
        return $stmt->execute();
    }
    
    public function delete($id) {
       $stmt = $this->db->prepare("DELETE FROM categorias WHERE id = :id");
       $stmt->bindParam(':id', $id); 
       return $stmt->execute();
    }
    
   

}
