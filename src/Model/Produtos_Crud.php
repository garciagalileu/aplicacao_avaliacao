<?php

namespace WebJump\Model;
require('vendor/database.php');
require('vendor/config.php');

class Produtos_Crud {

    public function insere($dados) {

        $pdo = new \PDO("mysql:host=".HOST.";"."dbname=".NOME_BANCO, SENHA_BANCO, "");
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $produtosDAO = new ProdutosDAO($pdo);
        
        if ($produtosDAO->insert($dados)) {
            header("Location: ". BASE_URL.'?r=products');
        } else {
            header("Location: ". BASE_URL.'?r=addProduct');
        }
    }
    
    public function atualiza($dados) {

        $pdo = new \PDO("mysql:host=".HOST.";"."dbname=".NOME_BANCO, SENHA_BANCO, "");
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $produtosDAO = new ProdutosDAO($pdo);
        
        if ($produtosDAO->update($dados)) {
            header("Location: ". BASE_URL.'?r=products');
        } else {
            header("Location: ". BASE_URL);
        }
    }
    
    public function seleciona() {

        $pdo = new \PDO("mysql:host=".HOST.";"."dbname=".NOME_BANCO, SENHA_BANCO, "");
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $produtosDAO = new ProdutosDAO($pdo);
        
        return $produtosDAO->select($n = null);
       
    }
    
    public function deleta() {
       
        $pdo = new \PDO("mysql:host=".HOST.";"."dbname=".NOME_BANCO, SENHA_BANCO, "");
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $produtosDAO = new ProdutosDAO($pdo);
        
        $id = filter_input_array(INPUT_POST)['id'];

        if ($produtosDAO->delete($id)) {
            $data = array('situacao' => 'true');
            echo json_encode($data);
        } else {
            $data = array('situacao' => 'false');
            echo json_encode($data);
        }
        
    }

    public function seleciona_filtro() {
       
        $pdo = new \PDO("mysql:host=".HOST.";"."dbname=".NOME_BANCO, SENHA_BANCO, "");
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $produtosDAO = new ProdutosDAO($pdo);
        
        $id = filter_input_array(INPUT_POST)['id'];
        
        if ($produtosDAO->select($id)) {
            session_start();
            $_SESSION['produtos'] = $produtosDAO->select($id);
            $data = array('situacao' => 'true');
            echo json_encode($data);
        } else {
            $data = array('situacao' => 'false');
            echo json_encode($data);
        }
        
    }
}
