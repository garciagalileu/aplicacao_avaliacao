<?php

namespace WebJump\Model;

require('vendor/autoload.php');
require('vendor/config.php');

use Rakit\Validation\Validator;

class Validacao {

    public function validaForm() {
        $validator = new Validator;
        $validacao = $validator->make($_POST, [
            'name' => 'required',
            'sku' => 'required|regex:/^\d{5}-\d{3}$/',
            'price' => 'required|regex:/^[0-9]\d{0,2}(\.\d{3})*,\d{2}$/',
            'quantity' => 'required|numeric',
            'description' => 'required',
            'category' => 'required',
        ]);

        $validacao->setMessages([
            'required' => 'Um ou mais campos não foram informados',
            'numeric' => 'O campo deve conter apenas números',
            'regex' => 'O campo preço foi informado incorretamente'
        ]);

        $validacao->validate();

        if ($validacao->fails()) {
           header("Location: ". BASE_URL.'?r=addProduct');
           exit();
        } else {
            return true;
        }
    }
    
    public function validaFormCategorias() {
        $validator = new Validator;
        $validacao = $validator->make($_POST, [
            'category-name' => 'required',
            'category-code' => 'required|numeric',
        ]);

        $validacao->setMessages([
            'required' => 'Um ou mais campos não foram informados',
            'numeric' => 'O campo deve conter apenas números',
        ]);

        $validacao->validate();

        if ($validacao->fails()) {
           header("Location: ". BASE_URL.'?r=addCategory');
           exit();
        } else {
            return true;
        }
    }

}
