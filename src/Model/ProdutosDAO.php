<?php

namespace WebJump\Model;

class ProdutosDAO {

    private $db;

    public function __construct(\PDO $pdo) {
        $this->db = $pdo;
    }

    public function insert($dados) {
        
        $stmt = $this->db->prepare("INSERT INTO produtos SET nome = :nome, sku = :sku, descricao = :descricao, quantidade = :quantidade, preco = :preco, categoria = :categoria, imagem = :imagem");
        $stmt->bindValue(':nome',$dados['nome']);
        $stmt->bindValue(':sku',$dados['sku']);
        $stmt->bindValue(':descricao', $dados['descricao']);
        $stmt->bindValue(':quantidade',$dados['quantidade']);
        $stmt->bindValue(':preco',str_replace(array('.', ','), array('', '.'),$dados['preco']));
        $stmt->bindValue(':categoria',$dados['categorias'][0]);
        $stmt->bindValue(':imagem', $dados['imagem']);
        return $stmt->execute();
    }
    public function select($n) {
        $result = ($n == null) ? $this->db->query("SELECT * FROM produtos;")->fetchAll() : $this->db->query("SELECT * FROM produtos WHERE id ='".$n."'")->fetchAll();
        return $result;
    }
    
    public function update($dados) {
        session_start();
        $stmt = $this->db->prepare("UPDATE produtos SET nome = :nome, sku = :sku, descricao = :descricao, quantidade = :quantidade, preco = :preco, categoria = :categoria, imagem = :imagem WHERE id = :id");
        $stmt->bindValue(':id', $_SESSION['produtos'][0]['id']);
        $stmt->bindValue(':nome',$dados['nome']);
        $stmt->bindValue(':sku',$dados['sku']);
        $stmt->bindValue(':descricao', $dados['descricao']);
        $stmt->bindValue(':quantidade',$dados['quantidade']);
        $stmt->bindValue(':preco',str_replace(array('.', ','), array('', '.'),$dados['preco']));
        $stmt->bindValue(':categoria',$dados['categorias'][0]);
        $stmt->bindValue(':imagem', $dados['imagem']);
        return $stmt->execute();
    }
    
    public function delete($id) {
       $stmt = $this->db->prepare("DELETE FROM produtos WHERE id = :id");
       $stmt->bindParam(':id', $id); 
       return $stmt->execute();
    }
    
   

}
