<?php require 'commons/cabecalho.php'; ?>
  <main class="content">
    <h1 class="title new-item">New Category</h1>
    <form method="POST" action="?r=cadastra_categoria">
      <div class="input-field">
        <label for="category-name" class="label">Category Name</label>
        <input type="text" id="category-name" name="category-name" autofocus="" class="input-text" />
        
      </div>
      <div class="input-field">
        <label for="category-code" class="label">Category Code</label>
        <input type="text" id="category-code" name="category-code" class="input-text" />
        
      </div>
      <div class="actions-form">
        <a href="categories.html" class="action back">Back</a>
        <input class="btn-submit btn-action"  type="submit" value="Save" />
      </div>
    </form>
  </main>
  <!-- Main Content -->
<?php require 'commons/rodape.php'; ?>
</body>

</html>
