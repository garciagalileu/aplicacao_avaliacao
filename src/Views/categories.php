<?php
require 'commons/cabecalho.php';
$categorias = $vars['categorias'];
?>
<main class="content">
    <div class="header-list-page">
        <h1 class="title">Categories</h1>
        <a href="?r=addCategory" class="btn-action">Add new Category</a>
    </div>
    <table class="data-grid">
        <tr class="data-row">
            <th class="data-grid-th">
                <span class="data-grid-cell-content">Name</span>
            </th>
            <th class="data-grid-th">
                <span class="data-grid-cell-content">Code</span>
            </th>
            <th class="data-grid-th">
                <span class="data-grid-cell-content">Actions</span>
            </th>
        </tr>
        <?php foreach ($categorias as $categoria) { ?>
            <tr class="data-row">
                <td class="data-grid-td">
                    <span class="data-grid-cell-content"><?= $categoria['nome'] ?></span>
                </td>

                <td class="data-grid-td">
                    <span class="data-grid-cell-content"><?= $categoria['codigo'] ?></span>
                </td>

                <td class="data-grid-td">
                    <div class="actions">
                        <div class="action edit" style="cursor: pointer" data-id="<?= $categoria['id'] ?>"><span>Edit</span></div>
                        <div class="action delete" style="cursor: pointer" data-id="<?= $categoria['id'] ?>"><span>Delete</span></div>
                    </div>
                </td>
            </tr>
        <?php } ?>
    </table>
</main>
<!-- Main Content -->
<?php require 'commons/rodape.php'; ?>
<script>
    $(document).ready(function () {
        $('.delete').on('click', function () {
            var id = $(this).attr("data-id");
            $.ajax({
                data: {id: id},
                type: "POST",
                dataType: 'json',
                url: '?r=deleta_categoria',
                success: function (data) {
                    if (data['situacao'] == 'true') {
                        window.location = '?r=categories';
                    }
                }
            });
        });

        $('.edit').on('click', function () {
            var id = $(this).attr("data-id");
            $.ajax({
                data: {id: id},
                type: "POST",
                dataType: 'json',
                url: '?r=seleciona_filtro_categoria',
                success: function (data) {
                    if (data['situacao'] == 'true') {
                        window.location = '?r=edita_categoria';
                    }
                }
            });
        });
    });
</script>
</body>

</html>
