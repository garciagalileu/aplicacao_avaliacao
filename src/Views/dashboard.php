<?php
require 'commons/cabecalho.php';
$produtos = $vars['produtos'];
?>

<!-- Header -->
<!-- Main Content -->
<main class="content">
    <div class="header-list-page">
        <h1 class="title">Dashboard</h1>
    </div>
    <div class="infor">
        <?php if (count($produtos) > 0) { ?>
            You have <?= count($produtos) ?> products added on this store: <a href="?r=addProduct" class="btn-action">Add new Product</a>
        <?php } else { ?>
            You don't have products added on this store: <a href="?r=addProduct" class="btn-action">Add new Product</a>
        <?php } ?>
    </div>
    <ul class="product-list">
        <?php if (count($produtos) > 0) { ?>
            <?php foreach ($produtos as $produto) { ?>
                <li>
                    <div class="product-image">
                        <?php if (empty($produto['imagem'])){ ?> 
                        <img src="assets/images/product/tenis-runner-bolt.png" layout="responsive" width="164" height="145" alt="Tênis Runner Bolt" />
                        <?php } else { ?>
                        <img src="assets/images/product/<?=$produto['imagem'] ?>" layout="responsive" width="164" height="145" alt="Tênis Runner Bolt" />                        
                        <?php } ?>
                    </div>
                    <div class="product-info">
                        <div class="product-name"><span><?= $produto['nome'] ?></span></div>
                        <div class="product-price"><span class="special-price"><?= $produto['quantidade'] ?> available</span> <span>R$ <?= number_format($produto['preco'], 2, ',', '.') ?></span></div>
                    </div>
                </li>
            <?php } ?>
        <?php } ?>
    </ul>
</main>
<!-- Main Content -->
<?php require 'commons/rodape.php'; ?>
</body>
</html>
