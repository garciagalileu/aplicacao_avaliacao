<?php require 'commons/cabecalho.php'; ?>

  <main class="content">
    <h1 class="title new-item">Edit Category</h1>
    <?php 
    if(isset($_SESSION['categorias'])) {
    $categorias =  $_SESSION['categorias'][0];
    ?>
    <form method="POST" action="?r=atualiza_categoria">
      <div class="input-field">
        <label for="category-name" class="label">Category Name</label>
        <input type="text" id="category-name" value="<?= $categorias['nome']?>" name="category-name" autofocus="" class="input-text" />
        
      </div>
      <div class="input-field">
        <label for="category-code" class="label">Category Code</label>
        <input type="text" id="category-code" value="<?= $categorias['codigo']?>" name="category-code" class="input-text" />
        
      </div>
      <div class="actions-form">
        <a href="categories.html" class="action back">Back</a>
        <input class="btn-submit btn-action"  type="submit" value="Save" />
      </div>
    </form>
    <?php } else { 
    header("Location: ". BASE_URL.'?r=categories');    
    }
    ?>
  </main>
  <!-- Main Content -->
<?php require 'commons/rodape.php'; ?>
</body>

</html>
