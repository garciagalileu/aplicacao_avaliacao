<?php
require 'commons/cabecalho.php';
$produtos = $vars['produtos'];
?>
<!-- Header --><body>
    <!-- Main Content -->
    <main class="content">
        <div class="header-list-page">
            <h1 class="title">Products</h1>
            <a href="?r=addProduct" class="btn-action">Add new Product</a>
        </div>
        <table class="data-grid">
            <tr class="data-row">
                <th class="data-grid-th">
                    <span class="data-grid-cell-content">Name</span>
                </th>
                <th class="data-grid-th">
                    <span class="data-grid-cell-content">SKU</span>
                </th>
                <th class="data-grid-th">
                    <span class="data-grid-cell-content">Price</span>
                </th>
                <th class="data-grid-th">
                    <span class="data-grid-cell-content">Quantity</span>
                </th>
                <th class="data-grid-th">
                    <span class="data-grid-cell-content">Categories</span>
                </th>

                <th class="data-grid-th">
                    <span class="data-grid-cell-content">Actions</span>
                </th>
            </tr>
            <?php foreach ($produtos as $produto) { ?>
                <tr class="data-row">
                    <td class="data-grid-td">
                        <span class="data-grid-cell-content"><?= $produto['nome'] ?></span>
                    </td>

                    <td class="data-grid-td">
                        <span class="data-grid-cell-content"><?= $produto['sku'] ?></span>
                    </td>

                    <td class="data-grid-td">
                        <span class="data-grid-cell-content">R$ <?= number_format($produto['preco'], 2, ',', '.') ?></span>
                    </td>

                    <td class="data-grid-td">
                        <span class="data-grid-cell-content"><?= $produto['quantidade'] ?></span>
                    </td>

                    <td class="data-grid-td">
                        <span class="data-grid-cell-content"><?= str_replace('|', '<br>', $produto['categoria']) ?></span>
                    </td>

                    <td class="data-grid-td">
                        <div class="actions">
                            <div class="action edit" style="cursor: pointer" data-id="<?= $produto['id'] ?>"><span>Edit</span></a></div>
                            <div class="action delete" style="cursor: pointer" data-id="<?= $produto['id'] ?>"><span>Delete</span></div>
                        </div>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </main>
    <!-- Main Content -->
    <?php require 'commons/rodape.php'; ?>
    <script>
        $(document).ready(function () {
            $('.delete').on('click', function () {
                var id = $(this).attr("data-id");
                $.ajax({
                    data: {id: id},
                    type: "POST",
                    dataType: 'json',
                    url: '?r=deleta',
                    success: function (data) {
                        if (data['situacao'] == 'true') {
                            window.location = '?r=products';
                        }
                    }
                });
            });
            
            $('.edit').on('click', function () {
                var id = $(this).attr("data-id");
                $.ajax({
                    data: {id: id},
                    type: "POST",
                    dataType: 'json',
                    url: '?r=seleciona_filtro',
                    success: function (data) {
                        if (data['situacao'] == 'true') {
                            window.location = '?r=edita';
                        }
                    }
                });
            });
        });
    </script>
</body>
</html>
