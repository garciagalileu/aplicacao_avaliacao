<?php
require 'commons/cabecalho.php';
$categorias = $vars['categorias'];
?>
<!-- Header -->
<!-- Main Content -->
<main class="content">
    <h1 class="title new-item">New Product</h1>
    <form method="POST" action="?r=cadastra" enctype="multipart/form-data">
        <div class="input-field">
            <label for="conteudo">Enviar imagem:</label>
            <input type="file" name="pic" accept="image/*" class="form-control">
        </div>
        <div class="input-field">
            <label for="sku" class="label">Product SKU</label>
            <input type="text" id="sku" name="sku" class="input-text" autofocus="" /> 
        </div>
        <div class="input-field">
            <label for="name" class="label">Product Name</label>
            <input type="text" id="name" name="name" class="input-text" /> 
        </div>
        <div class="input-field">
            <label for="price" class="label">Price</label>
            <input type="text" id="price" name="price" class="input-text" /> 
        </div>
        <div class="input-field">
            <label for="quantity" class="label">Quantity</label>
            <input type="text" id="quantity" name="quantity" class="input-text" /> 
        </div>
        <div class="input-field">
            <label for="category" class="label">Categories</label>
            <select multiple id="category" name="category[]" class="input-text">
                <?php foreach ($categorias as $categoria) { ?>
                    <option value="<?= $categoria['nome'] ?>"><?= $categoria['nome'] ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="input-field">
            <label for="description" class="label">Description</label>
            <textarea id="description" name="description" class="input-text"></textarea>
        </div>

        <div class="actions-form">
            <a href="products.html" class="action back">Back</a>
            <input class="btn-submit btn-action" type="submit" value="Save Product" />
        </div>

    </form>
</main>
<!-- Main Content -->
<?php require 'commons/rodape.php'; ?>
</body>
<script>
    $(document).ready(function () {
        $('#price').mask('000.000.000.000.000,00', {reverse: true});
        $('#sku').mask('00000-000');
    });
</script>
</html>
