<?php

namespace WebJump\Controller;

use WebJump\Model\Produtos_Crud;
use WebJump\Model\Categorias_Crud;

class WebJump extends Controller {
    
    public static function index() {
        $produtosCRUD = new Produtos_Crud();
        $obtemProdutos = $produtosCRUD->seleciona();
        return self::view('dashboard',['produtos' => $obtemProdutos]);
    }

    public function addProduct() {
        $categoriasCRUD = new Categorias_Crud();
        $obtemCategorias = $categoriasCRUD->seleciona();
        return self::view('addProduct',['categorias' => $obtemCategorias]);
    }
    
    public function products() {
        $produtosCRUD = new Produtos_Crud();
        $obtemProdutos = $produtosCRUD->seleciona();
        session_start();
        unset($_SESSION['produtos']);
        session_destroy();
        return self::view('products',['produtos' => $obtemProdutos]);
    }
    
    public function edita() {
        session_start();
        $categoriasCRUD = new Categorias_Crud();
        $obtemCategorias = $categoriasCRUD->seleciona();
        return self::view('products_update', ['categorias' => $obtemCategorias]);
    }
    
    public function categories() {
        $categoriasCRUD = new Categorias_Crud();
        $obtemCategorias = $categoriasCRUD->seleciona();
        session_start();
        unset($_SESSION['categorias']);
        session_destroy();
        return self::view('categories',['categorias' => $obtemCategorias]);
    }
    
    public function addCategory() {
        return self::view('addCategory');
    }
    
     public function edita_categoria() {
        session_start();
        return self::view('categories_update');
    }
}
