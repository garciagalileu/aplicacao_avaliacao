<?php

namespace WebJump\Controller;

use WebJump\Controller\Produtos;
use WebJump\Model\Produtos_Crud;

class Produtos_Form extends Controller {

    public function createForm() {

        $post = filter_input_array(INPUT_POST);
        $dir = str_replace('\src\Controller', '', __DIR__);

        $produtos = new Produtos();
        $produtos->setNome($post['name']);
        $produtos->setSku($post['sku']);
        $produtos->setDescricao($post['description']);
        $produtos->setQuantidade($post['quantity']);
        $produtos->setPreco($post['price']);
        if (!empty($_FILES['pic']['name'])) {
            $ext = strtolower(substr($_FILES['pic']['name'], -4));
            $novo_nome = rand() . $ext;
            $diretorio = $dir . '/assets/images/product/';
            move_uploaded_file($_FILES['pic']['tmp_name'], $diretorio . $novo_nome);
        }
        (!empty($_FILES['pic'])) ? $img = $novo_nome : $img = NULL;
        $dados = [
            'nome' => $produtos->getNome(),
            'sku' => $produtos->getSku(),
            'descricao' => $produtos->getDescricao(),
            'quantidade' => $produtos->getQuantidade(),
            'preco' => $produtos->getPreco(),
            'imagem' => $img,
            'categorias' => []
        ];

        $validacao = new \WebJump\Model\Validacao();
        $validacao->validaForm($dados);
        $produtos->setCategorias($post['category']);
        $itens_cat = '';
        $categoria = $produtos->getCategorias();

        foreach ($categoria as $categorias) {
            $itens_cat .= $categorias . '|';
        }
        $corrige_itens_cat = substr($itens_cat, 0, -1);
        array_push($dados['categorias'], $corrige_itens_cat);

        $produtosCRUD = new Produtos_Crud();
        $produtosCRUD->insere($dados);
    }

    public function editaForm() {

        $post = filter_input_array(INPUT_POST);
        $dir = str_replace('\src\Controller', '', __DIR__);

        $produtos = new Produtos();
        $produtos->setNome($post['name']);
        $produtos->setSku($post['sku']);
        $produtos->setDescricao($post['description']);
        $produtos->setQuantidade($post['quantity']);
        $produtos->setPreco($post['price']);
        if (!empty($_FILES['pic']['name'])) {
            $ext = strtolower(substr($_FILES['pic']['name'], -4));
            $novo_nome = rand() . $ext;
            $diretorio = $dir . '/assets/images/product/';
            move_uploaded_file($_FILES['pic']['tmp_name'], $diretorio . $novo_nome);
        }
        (!empty($_FILES['pic'])) ? $img = $novo_nome : $img = NULL;
        $dados = [
            'nome' => $produtos->getNome(),
            'sku' => $produtos->getSku(),
            'descricao' => $produtos->getDescricao(),
            'quantidade' => $produtos->getQuantidade(),
            'preco' => $produtos->getPreco(),
            'imagem' => $img,
            'categorias' => []
        ];

        $validacao = new \WebJump\Model\Validacao();
        $validacao->validaForm($dados);
        $produtos->setCategorias($post['category']);
        $itens_cat = '';
        $categoria = $produtos->getCategorias();

        foreach ($categoria as $categorias) {
            $itens_cat .= $categorias . '|';
        }
        $corrige_itens_cat = substr($itens_cat, 0, -1);
        array_push($dados['categorias'], $corrige_itens_cat);

        $produtosCRUD = new Produtos_Crud();
        $produtosCRUD->atualiza($dados);
    }

}
