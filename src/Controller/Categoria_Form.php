<?php

namespace WebJump\Controller;

use WebJump\Controller\Categorias;
use WebJump\Model\Categorias_Crud;

class Categoria_Form extends Controller {

    public function createForm() {

        $post = filter_input_array(INPUT_POST);
        $categorias = new Categorias();
        $categorias->setNome($post['category-name']);
        $categorias->setCodigo($post['category-code']);


        $dados = [
            'nome' => $categorias->getNome(),
            'codigo' => $categorias->getCodigo(),
        ];

        $validacao = new \WebJump\Model\Validacao();
        $validacao->validaFormCategorias($dados);

        $categoriasCRUD = new Categorias_Crud();
        $categoriasCRUD->insere($dados);
    }

    public function editaForm() {

        $post = filter_input_array(INPUT_POST);
        $categorias = new Categorias();
        $categorias->setNome($post['category-name']);
        $categorias->setCodigo($post['category-code']);


        $dados = [
            'nome' => $categorias->getNome(),
            'codigo' => $categorias->getCodigo(),
        ];
        
        $validacao = new \WebJump\Model\Validacao();
        $validacao->validaFormCategorias($dados);

        $categoriasCRUD = new Categorias_Crud();
        $categoriasCRUD->atualiza($dados);
    }

}
