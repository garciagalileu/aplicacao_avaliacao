<?php

namespace WebJump\Controller;

class Produtos {

    private $nome;
    private $sku;
    private $descricao;
    private $quantidade;
    private $preco;
    private $categorias;

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function setSku($sku) {
        $this->sku = $sku;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    public function setQuantidade($quantidade) {
        $this->quantidade = $quantidade;
    }

    public function setPreco($preco) {
        $this->preco = $preco;
    }

    public function setCategorias($categorias) {
        $this->categorias = $categorias;
    }

    public function getNome() {
        return $this->nome;
    }

    public function getSku() {
        return $this->sku;
    }

    public function getDescricao() {
        return $this->descricao;
    }

    public function getQuantidade() {
        return $this->quantidade;
    }

    public function getPreco() {
        return $this->preco;
    }

    public function getCategorias() {
        return $this->categorias;
    }

}
