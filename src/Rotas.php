<?php

namespace WebJump;

class Rotas {
 
    private $routes = [];
    private static $params = [];

    private function validar(string $method) {
        return in_array($method, ['get', 'post']);
    }

    public function __call(string $method, array $args) {
        $metodo = strtolower($method);

        if (!$this->validar($metodo)) {
            return false;
        }
        
        [$route, $action] = $args;
        
        if (!isset($action) or!is_callable($action)) {
            return false;
        }
        
        $this->routes[$method][$route] = $action;
        
        return true;
    }

    public function run() {
        
        $method = strtolower(filter_input(INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_STRING)) ?? 'get';
        $route = $_GET['r'] ?? '/';
        
        if (!isset($this->routes[$method]) || !isset($this->routes[$method][$route]) ) {
            die('Ocorreu um erro ao executar a aplicação');
        }
        
        self::$params = $this->getParams($method);

        die($this->routes[$method][$route]());
    }

    private function getParams(string $method) {
        if ($method == 'get') {
            return filter_input_array(INPUT_GET);
        }    
        return filter_input_array(INPUT_POST);
    }

    public static function getRequest() {
        return self::$params;
    }

}
