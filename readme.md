# Documentação do Projeto

# Instruções
- A primeira coisa que vamos precisar é de um servidor para executar a aplicação. Você pode utilizar uma suite completa como o xampp - que já conta com o banco de dados - para esta aplicação.
- Agora que temos o servidor, o próximo passo é realizar o download de todos os arquivos que estão aqui para o local onde a aplicação deve ser executada.
- O próximo passo para utilizar este projeto é criação do banco de dados que servirá de apoio para a aplicação. O nome do banco de dados deve ser webjump.
- O próximo passo é realizar a importação das tabelas para o banco de dados. Você deve usar o arquivo webjump.sql e realizar a importação através do phpmyadmin, por exemplo.
- Agora que o banco de dados já está disponível e a aplicação está no local onde deve ser executada, é só executar a aplicação. É isso!

# O que foi utilizado neste projeto?
A partir dos requisitos estabelecidos, neste projeto foi adotado o padrão MVC (Model, View e Controller), SOLID e algumas bibliotecas para validação do formulários e para a máscara em alguns inputs do formulário. Dessa forma, a aplicação se torna mais simples e intuitiva de ser utilizada.
Neste projeto também foi utilizado do Composer para facilicar o desenvolvimente e a obtenção das bibliotecas

# Requisitos do Projeto
- O sistema deverá ser desenvolvido utilizando a linguagem PHP (de preferência a versão mais nova) ou outra linguagem se assim foi especificado para sua avaliação por nossa equipe.
- Você deve criar um CRUD que permita cadastrar as seguintes informações:
	- **Produto**: Nome, SKU (Código), preço, descrição, quantidade e categoria (cada produto pode conter uma ou mais categorias)
	- **Categoria**: Código e nome.
- Salvar as informações necessárias em um banco de dados (relacional ou não), de sua escolha

# Opcionais
- Gerar logs das ações
- Testes automatizados com informação da cobertura de testes
- Upload de imagem no cadastro de produtos

Qualquer dúvida sobre o teste, fique a vontade para entrar em contato.